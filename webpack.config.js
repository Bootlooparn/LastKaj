const path = require('path')
require("@babel/polyfill")

const entry = path.join(__dirname, '/src/App.js')
const outputPath = path.join(__dirname, '/dist')
const outputFile = 'bundle.js'

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = (env) => {
    const isProd = env === 'production'
    return {
        entry: ["@babel/polyfill", entry],
        output: {
            path: outputPath,
            filename: outputFile
        },
        module: {
            rules: [{
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/,
            },
            {
                loader: ['style-loader', 'css-loader', 'sass-loader'],
                test: /\.(scss|css)$/
            },
            {
                loader: ['url-loader'],
                test: /\.(png|jpg|gif|ttf|eot|woff|woff2|svg)$/
            }
            ]
        },
        devtool: isProd ? 'source-map': 'eval',
        devServer: {
            contentBase: outputPath
        },
        plugins:[
            new BundleAnalyzerPlugin(),
            new CompressionPlugin()
        ]
    }
}