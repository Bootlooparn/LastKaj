import axios from 'axios'

// Types:
const actionTypes = {
    ADD_TOKEN: 'ADD_TOKEN',
    UPDATE_ITEMS: 'UPDATE_ITEMS',
    AUTHORIZE_USER: 'AUTHORIZE_USER',
    ERROR: 'ERROR'
}

// Action creators
export const loginUser = (user, history) => {

    const request = axios.post('http://lastkaj.com/login', user)

    return (dispatch) => {
        request.then((response) => {
            dispatch({ type: actionTypes.ADD_TOKEN, token: response.data })
            history.push("/Register")
        }).catch(
            (error) => {
                const errorItem = {
                    data: error.response.data,
                    status: error.response.status
                }

                dispatch(errorCatcher(errorItem))
                throw error
            }
        )
    }
}

export const updateItems = (item, token, typeRequest) => {
    const request = axios.post('http://lastkaj.com/items', { item: item, token: token, typeRequest: typeRequest })

    return (dispatch) => {
        request.then((response) => {
            dispatch({ type: actionTypes.UPDATE_ITEMS, item: response.data })
        }).catch(
            (error) => {
                return error
            }
        )
    }
}

export const authorizeUser = (token, history) => {
    const request = axios.post('http://lastkaj.com/authorization', { token: token })

    return () => {
        request.then((response) => {
        }).catch(
            (error) => {
                history.push('/')
            }
        )
    }
}

export const errorCatcher = (error) => {
    return {
        type: actionTypes.ERROR,
        error: error
    }
}

export default actionTypes