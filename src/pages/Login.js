import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loginUser } from '../actions/actions'

// Imported components
import InputAdornment from '@material-ui/core/InputAdornment'
import TextField from '@material-ui/core/TextField'
import IconButton from '@material-ui/core/IconButton'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'

// Imported icons
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import AccountCircle from '@material-ui/icons/AccountCircle'


// Imported styles
import '../styles/Login.scss'

class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {
        username: "",
        password: ""
      }
    }

    // Binding methods
    this.loginHandler = this.loginHandler.bind(this)
    this.inputHandler = this.inputHandler.bind(this)
  }

  inputHandler(e) {
    let value = e.target.value
    let newState = Object.assign({}, this.state)

    switch (e.target.name) {
      case 'username':
        newState.user.username = value
        this.setState(newState)
        break;
      case 'password':
        newState.user.password = value
        this.setState(newState)
        break;
      default:
        console.log('Error')
        break;
    }
  }

  loginHandler(e) {

    e.preventDefault()
    const history = this.props.history

    this.props.updateToken(this.state.user, this.props.history)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.error != this.props.error) {
      let newState = Object.assign({}, this.state)
      newState.user.username = ""
      newState.user.password = ""

      this.setState(newState)
    }
  }

  render() {
    return (
      <Grid container direction="row" justify="center" alignItems="center" className="adjust-height-login background">
        <Grid item container direction="row" justify="center" alignItems="center" id="login-box">
          <form autoComplete="off" onSubmit={this.loginHandler}>
            <TextField
              name="username"
              variant="outlined"
              margin="normal"
              type="text"
              label="Username"
              value={this.state.user.username}
              helperText={(this.props.error.status == 403) ? "Wrong username or password" : ""}
              onChange={this.inputHandler}
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility">
                      <AccountCircle />
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            <br />
            <TextField
              name="password"
              variant="outlined"
              margin="normal"
              type="password"
              label="Password"
              value={this.state.user.password}
              onChange={this.inputHandler}
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility">
                      {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            <Grid container direction="row" justify="center">
              <Button id="login-button" type="submit" variant="contained" size="large" fullWidth={true} color="primary">Login</Button>
            </Grid>
          </form>
        </Grid>
      </Grid>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    error: state.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateToken: (user, history) => {
      dispatch(loginUser(user, history))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)