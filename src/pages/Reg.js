import React, { Component } from 'react'
import { connect } from 'react-redux'
import { authorizeUser, updateItems } from '../actions/actions'

// Imported components
import MUIDataTable from 'mui-datatables'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'

// Imported styles
import '../styles/Reg.scss'

class Reg extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalState: false,
      modalForm: {
        company: '',
        adress: '',
        lastplats: '',
        width: '',
        height: '',
        instructions: '',
        index: ''
      },
      formInput: {
        company: '',
        adress: '',
        lastplats: '',
        width: '',
        height: '',
        instructions: ''
      },
      tableOptions: {
        print: false,
        download: false,
        filter: true,
        filterType: 'multiselect',
        viewColumns: false,
        onRowClick: (rowData, rowMeta) => {
          setTimeout(() => {
            if (this.state.tableInfo.cellSelected != false) {
              this.modalCall("table", rowData, rowMeta)
              let newState = Object.assign({}, this.state)
              newState.tableInfo.cellSelected = !this.state.tableInfo.cellSelected
            }
          }, 100)
        },
        onCellClick: (colData, cellMeta) => {
          let newState = Object.assign({}, this.state)
          newState.tableInfo.cellSelected = !this.state.tableInfo.cellSelected
          this.setState(newState)
        },
        onRowsDelete: (rowsDeleted) => {
          var promises = []
          rowsDeleted.data.map((i) => {
            return promises.push(this.props.items.data[i.dataIndex].slice(0, 3))
          })
          Promise.all(promises).then((result) => {
            this.props.itemUpdate(result, this.props.token, "remove")
          })
        }
      },
      tableInfo: {
        cellSelected: false
      }
    }

    // Binding methods
    this.inputHandler = this.inputHandler.bind(this)
    this.submitItem = this.submitItem.bind(this)
    this.modalCall = this.modalCall.bind(this)
    this.modalInputHandler = this.modalInputHandler.bind(this)
  }

  componentWillMount() {
    this.props.auth(this.props.token, this.props.history)
    this.props.itemUpdate([], this.props.token, "get")
  }
  inputHandler(e) {
    let value = e.target.value
    let newState = Object.assign({}, this.state)

    switch (e.target.name) {
      case 'company':
        newState.formInput.company = value
        this.setState(newState)
        break
      case 'adress':
        newState.formInput.adress = value
        this.setState(newState)
        break
      case 'lastplats':
        newState.formInput.lastplats = value
        this.setState(newState)
        break
      case 'width':
        newState.formInput.width = value
        this.setState(newState)
        break
      case 'height':
        newState.formInput.height = value
        this.setState(newState)
        break
      case 'instructions':
        newState.formInput.instructions = value
        this.setState(newState)
        break
      default:
        break
    }
  }
  modalInputHandler(e) {
    let value = e.target.value
    let newState = Object.assign({}, this.state)

    switch (e.target.name) {
      case 'company':
        newState.modalForm.company = value
        this.setState(newState)
        break
      case 'adress':
        newState.modalForm.adress = value
        this.setState(newState)
        break
      case 'width':
        newState.modalForm.width = value
        this.setState(newState)
        break
      case 'height':
        newState.modalForm.height = value
        this.setState(newState)
        break
      case 'instructions':
        newState.modalForm.instructions = value
        this.setState(newState)
        break
      default:
        break
    }
  }
  submitItem(e) {
    e.preventDefault()
    const promise = new Promise((resolve, reject) => {
      resolve(this.props.itemUpdate(this.state.formInput, this.props.token, "submit"))
    })

    promise.then(() => {
      let newState = Object.assign({}, this.state)

      newState.formInput.company = ""
      newState.formInput.adress = ""
      newState.formInput.lastplats = ""
      newState.formInput.width = ""
      newState.formInput.height = ""
      newState.formInput.instructions = ""

      this.setState(newState)

    })

  }

  modalCall(caller, rowData, rowMeta) {
    let newState = Object.assign({}, this.state)
    switch (caller) {
      case "table": {
        const index = rowMeta.rowIndex
        const [company, adress, lastplats, width, height, instructions] = rowData
        newState.modalForm = {
          company: company,
          adress: adress,
          lastplats: lastplats,
          width: width,
          height: height,
          instructions: instructions,
          index: index
        }
        newState.modalState = !this.state.modalState
        this.setState(newState)
      }
        break
      case "update": {
        const item = this.state.modalForm
        this.props.itemUpdate(item, this.props.token, "update")
        newState.modalState = !this.state.modalState
        this.setState(newState)
      }
        break
      default: {
        newState.modalState = !this.state.modalState
        this.setState(newState)
      }
        break
    }
  }

  render() {
    return (
      <Grid className="adjust-title" container direction="column" justify="flex-start" alignItems="center" spacing={40}>
        <Grid item container direction="column" justify="space-evenly" alignItems="center">
          <Grid item>
            <Typography variant="display1">LastKaj</Typography>
          </Grid>
          <form className="adjust-form" autoComplete="off" onSubmit={this.submitItem}>
            <TextField name="company" label="Company" margin="normal" variant="outlined" value={this.state.formInput.company} onChange={this.inputHandler} fullWidth required />
            <br />
            <TextField name="adress" label="Adress" margin="normal" variant="outlined" value={this.state.formInput.adress} onChange={this.inputHandler} fullWidth required />
            <br />
            <TextField name="lastplats" label="Adress (Lastplats)" margin="normal" variant="outlined" value={this.state.formInput.lastplats} onChange={this.inputHandler} fullWidth required />
            <br />
            <TextField name="width" label="Width" margin="normal" variant="outlined" type="number" value={this.state.formInput.width} onChange={this.inputHandler} fullWidth required />
            <br />
            <TextField name="height" label="Height" margin="normal" variant="outlined" type="number" value={this.state.formInput.height} onChange={this.inputHandler} fullWidth required />
            <br />
            <TextField name="instructions" label="Instructions" margin="normal" variant="outlined" value={this.state.formInput.instructions} onChange={this.inputHandler} rows="3" multiline fullWidth required />
            <Button type="submit" fullWidth variant="contained" color="primary" className="adjust-button">Submit</Button>
          </form>
        </Grid>
        <Grid item>
          <MUIDataTable className="adjust-table" data={this.props.items.data} columns={this.props.items.columns} options={this.state.tableOptions} />
        </Grid>
        <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={this.state.modalState}>
          <Grid className="adjust-height" container direction="column" justify="space-evenly" alignItems="center">
            <Grid item>
              <Paper className="modal-size" square elevation={1}>
                <Grid container direction="column" justify="flex-start" alignItems="center">
                  <form autoComplete="off" className="modal-adjust-form" onSubmit={(e) => { e.preventDefault(); this.modalCall("update") }}>
                    <Grid>
                      <Typography variant="title">Modify</Typography>
                    </Grid>
                    <TextField name="company" label="Company" margin="normal" variant="outlined" value={this.state.modalForm.company} onChange={this.modalInputHandler} fullWidth required />
                    <br />
                    <TextField name="adress" label="Adress" margin="normal" variant="outlined" value={this.state.modalForm.adress} onChange={this.modalInputHandler} fullWidth required />
                    <br />
                    <TextField name="adress" label="Adress" margin="normal" variant="outlined" value={this.state.modalForm.lastplats} onChange={this.modalInputHandler} fullWidth required />
                    <br />
                    <TextField name="width" label="Width" margin="normal" variant="outlined" value={this.state.modalForm.width} onChange={this.modalInputHandler} fullWidth required />
                    <br />
                    <TextField name="height" label="Height" margin="normal" variant="outlined" value={this.state.modalForm.height} onChange={this.modalInputHandler} fullWidth required />
                    <br />
                    <TextField name="instructions" label="Instructions" margin="normal" variant="outlined" value={this.state.modalForm.instructions} onChange={this.modalInputHandler} rows="3" multiline fullWidth required />
                    <Grid container direction="row" justify="center" alignItems="center">
                      <Button onClick={this.modalCall}>Close</Button>
                      <Button type="submit">Submit</Button>
                    </Grid>
                  </form>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </Modal>
      </Grid>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    token: state.token,
    items: state.items
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    auth: (token, history) => {
      dispatch(authorizeUser(token, history))
    },
    itemUpdate: (item, token, typeRequest) => {
      dispatch(updateItems(item, token, typeRequest))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reg)