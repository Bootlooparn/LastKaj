// Essential imports
import React from 'react'
import ReactDOM from 'react-dom'

// Library imports
import { BrowserRouter as Router, Route } from "react-router-dom"
import { Provider } from 'react-redux'

// Imported components
import Grid from '@material-ui/core/Grid'

// Import store
import store from './store/store'

// Imported pages
import Login from './pages/Login'
import Reg from './pages/Reg'

// Imported styles
import './styles/App.scss'
import '../node_modules/bootstrap-css-only/css/bootstrap.min.css'

////--------

const unsubscribe = store.subscribe(
    () => {
        console.log(store.getState())
    }
)

unsubscribe()

const template = (
    <Provider store={store}>
        <Grid>
            <Router>
                <div>
                    <Route exact path="/" component={Login} />
                    <Route path="/Register" component={Reg} />
                </div>
            </Router>
        </Grid>
    </Provider>
);

ReactDOM.render(template, document.getElementById('App'));