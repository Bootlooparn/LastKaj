import { combineReducers } from 'redux'
import actionTypes from '../actions/actions'

const tokenReducer = (state = [], action) => {
    switch (action.type) {
        case actionTypes.ADD_TOKEN:
            return action.token
            break
        default:
            return state
    }
}

const itemReducer = (state = [], action) => {
    switch (action.type) {
        case actionTypes.UPDATE_ITEMS:
            return action.item
            break
        default:
            return state
    }
}

const errorReducer = (state = [], action) => {
    switch (action.type) {
        case actionTypes.ERROR:
            return action.error
        default:
            return state
    }
}

const rootReducers = combineReducers({
    token: tokenReducer,
    items: itemReducer,
    error: errorReducer
})

export default rootReducers