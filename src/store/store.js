import ReduxThunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducers from '../reducers/reducers'

export const initialState = {
    token: {},
    items: {
    },
    error: {
        data: '',
        status: ''
    }
}

// Store
const store = createStore(rootReducers, initialState, composeWithDevTools(applyMiddleware(ReduxThunk)))

export default store