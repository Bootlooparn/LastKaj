// Essential packages
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const path = require('path')
const database = require('./db/database')

const app = express()

authDB = database[1]
apiDB = database[0]

authDB.connect()
apiDB.connect()

// Middleware used
app.use(cors())
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, '/dist')))

authorization = (req, res, next) => {
    const token = req.body.token
    jwt.verify(token, 'ImWatchingYou', (err, decode) => {
        if (err) {
            res.sendStatus(403)
        } else {
            next()
        }
    })
}

// DB calls
var select = (dataSource, res) => {
    apiDB.query('SELECT company, adress, lastplats, width, height, instruction FROM ??', ['kajer'], (err, rows, fields) => {
        if (!err) {
            var promises = []
            for (let i = 0; i < rows.length; i++) {
                promises.push([rows[i].company, rows[i].adress, rows[i].lastplats, rows[i].width, rows[i].height, rows[i].instruction])
            }
            Promise.all(promises).then((result) => {
                dataSource.data = result
            }).then(() => {
                res.json(dataSource)
            })
        } else {
            res.sendStatus(500)
            throw err
        }
    })
}

//// Server startup
app.listen(3000, () => console.log('Up and running'))

//// Api calls

// Get calls
app.get('/', (req, res) => {
    res.sendFile('index.html')
})

app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    next();
})

// Post calls
app.post('/login', (req, res) => {
    const user = req.body
    authDB.query('SELECT COUNT(*) as count FROM ?? WHERE username = ?', ['users', user.username], (err, rows, fields) => {
        if (!err) {
            if (rows[0].count == 1) {
                authDB.query('SELECT COUNT(*) AS count FROM ?? WHERE username = ? && password = SHA(?)', ['users', user.username, user.password], (err, rows, fields) => {
                    if (!err) {
                        if (rows[0].count == 1) {
                            jwt.sign(user, 'ImWatchingYou', (err, token) => {
                                if (!err) {
                                    res.json(token)
                                } else {
                                    res.sendStatus(500)
                                    throw err
                                }
                            })
                        } else {
                            res.sendStatus(403)
                        }
                    } else {
                        res.sendStatus(500)
                        throw err
                    }
                })
            } else {
                res.sendStatus(403)
            }
        } else {
            res.sendStatus(500)
            throw err
        }
    })
})

app.post('/authorization', authorization, (req, res) => {
    res.sendStatus(200)
})

app.post('/items', authorization, (req, res) => {
    switch (req.body.typeRequest) {
        case "get":
            select(dataSource, res)
            break
        case "update":
            apiDB.query('UPDATE ?? SET company = ?, adress = ?, lastplats = ?, width = ?, height = ?, instruction = ? WHERE company = ? AND adress = ? AND lastplats = ?', ['kajer', req.body.item.company, req.body.item.adress, req.body.item.lastplats, req.body.item.width, req.body.item.height, req.body.item.instructions, req.body.item.company, req.body.item.adress, req.body.item.lastplats], (err) => {
                if (!err) {
                    select(dataSource, res)
                } else {
                    res.sendStatus(500)
                    throw err
                }
            })
            break
        case "remove":
            const promise = new Promise((resolve, reject) => {
                resolve(req.body.item.forEach(element => {
                    apiDB.query('DELETE FROM ?? WHERE company = ? AND adress = ? AND lastplats = ?', ['kajer', element[0], element[1], element[2]])
                }))
            })
            promise.then(() => {
                res.sendStatus(200)
            })
            break
        case "submit":
            apiDB.query('INSERT INTO ?? (company, adress, lastplats, width, height, instruction) VALUES (?, ?, ?, ?, ?, ?)', ['kajer', req.body.item.company, req.body.item.adress, req.body.item.lastplats, req.body.item.width, req.body.item.height, req.body.item.instructions], () => {
                select(dataSource, res)
            })
            break
        default:
            res.send('What ya looking for?')
            break
    }
})

// Data to be passed

const dataSource = {
    columns: [
        {
            name: "Company",
            options: {
            }
        },
        {
            name: "Adress",
            options: {
                filter: false
            }
        },
        {
            name: "Adress (Lastplats)",
            options: {
                filter: false
            }
        },
        {
            name: "Width",
            options: {
                filter: false
            }
        },
        {
            name: "Height",
            options: {
                filter: false
            }
        },
        {
            name: "Instructions",
            options: {
                filter: false
            }
        }
    ],
    data: [
    ]
}

app.all('*', (req, res) => {
    res.redirect('/')
})